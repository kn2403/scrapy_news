
from scrapy.crawler import CrawlerProcess
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor


import argparse

"""This is the script to start the spider, please run it with spider name input"""

parser = argparse.ArgumentParser(
    description='Indicate name of spider',
)


parser.add_argument('sname', action="store")
parser.add_argument('number', action="store")
parser.add_argument('delay', action="store")
spider=vars(parser.parse_args())
name=spider['sname']
num=int(spider['number'])
delay= int(spider['delay'])


settings = get_project_settings()
settings.set('CLOSESPIDER_ITEMCOUNT', num)
settings.set('LOG_EVERY', delay)


process = CrawlerProcess(settings)
runner = CrawlerRunner(settings)

to_crawl = ['reuters','time','nytimes','guardian','cnbc']

    

if name=="allspiders":
    

    for spider in to_crawl:
        runner.crawl(spider)

    d = runner.join()
    d.addBoth(lambda _: reactor.stop())
    reactor.run()

    

elif name in to_crawl:

    process.crawl(name,log_every=delay)
    process.start()

else:
    print("Please enter a valid spider")
