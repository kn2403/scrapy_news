from scrapy.exceptions import DropItem
from ftfy import fix_text
import logging


class ValidatePipeline(object):
    def __init__(self,log_every):
        
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
       
        logger = logging.getLogger('first logger')
        logger.setLevel(logging.INFO)
        if not logger.handlers:
            log = logging.StreamHandler()
            log.setLevel(logging.INFO)
            log.setFormatter(formatter)
            logger.addHandler(log)
        self.logger_first = logger
        self.count = 0
        self.count_drop = 0


        logger2 = logging.getLogger("second logger")
        logger2.setLevel(logging.INFO)
        if not logger2.handlers:
            handler = logging.FileHandler("log.txt")
            handler.setFormatter(formatter)
            
            logger2.addHandler(handler)
        self.logger_second = logger2
        self.log_every = log_every

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            log_every = crawler.settings.get('LOG_EVERY')
           
        )
        

    def process_item(self,item,spider):
        item = dict(item)
        req = ["doc_id","title","text","description","published_timestamp","published_date","crawled_timestamp","crawled_date","url","topic","language","source_type","source_name","domain"]

 

            #catch the missing item
        if not all([item[x] for x in req]):
            self.count_drop = self.count_drop + 1
            msg2 = ''.join(["Drop url:",item['url']])
            self.logger_second.info(msg2)

            raise DropItem("Missing Fields!".format(item))
        else:
            self.count = self.count +1
            item["title"] = fix_text(item["title"])
            item["text"] = fix_text(item["text"])
            item["description"] = fix_text(item["description"])
            
            if self.count%self.log_every ==0:
                
                msg = ' '.join([str(self.count),"items from",item['domain'] ,"added to database,",str(self.count_drop),"items dropped"])
                self.logger_first.info(msg)
            
            return item

        
