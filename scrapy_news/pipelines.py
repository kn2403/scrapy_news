# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from scrapy_news import settings
from scrapy.utils.log import configure_logging
from scrapy.exceptions import DropItem
import logging
from scrapy.exceptions import CloseSpider

class MongoDBPipeline(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings.MONGODB_SERVER,
            settings.MONGODB_PORT
        )
        
        db = connection[settings.MONGODB_DB]
        try:
            if db.authenticate(settings.MONGODB_USERNAME,settings.MONGODB_PASSWORD)==True:
                self.collection = db[settings.MONGODB_COLLECTION]
        except Exception as e:
            raise CloseSpider('Authentication fails!')



        

    def process_item(self, item, spider):
       
        self.collection.replace_one({"doc_id":item['doc_id'],"domain":item['domain']},item,upsert=True)
           
        return item



