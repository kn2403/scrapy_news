from time import strftime,strptime,mktime
import datetime
from dateutil.parser import parse
from scrapy_news.rules.rules import ArticleRules
import re
import pytz


class CNBCRule(ArticleRules):
    def __init__(self,response):
        self.response = response
   
    def parse_id(self):
        doc_id = ""
        try:
            doc_id = self.response.xpath('//meta[@property="pageNodeId"]/@content')[0].extract()
        except IndexError:
            pass
        return doc_id

    def parse_title(self):
        title = ""
        try:
            title = self.response.xpath('//meta[@property="og:title"]/@content')[0].extract()
            title = re.sub(r'&#\d;','',title)
        except IndexError:
            pass
        return title

    def parse_description(self):
        description = ""
        try:
            description = self.response.xpath('//meta[@name="description"]/@content')[0].extract()
        except IndexError:
            pass
        return description

    def parse_url(self):
        try:
            url = self.response.xpath('//meta[@property="og:url"]/@content')[0].extract()
        except IndexError:
            pass
        return url

    def parse_tags(self):
        tags = ""
        try:
            tags = self.response.xpath('//meta[@property="article:tag"]/@content').extract()
            
        except IndexError:
            pass
        return tags
   

    def parse_language(self):
        language = self.response.xpath('//meta[@itemprop="inLanguage"]/@content')[0].extract()
        return language

    def parse_image(self):
        image = ""
        try:
            image = self.response.xpath('//meta[@property="og:image"]/@content')[0].extract()
        except IndexError:
            pass
        return image

    def parse_text(self):
        text = ""
        try:
            text = self.response.xpath('//div[@itemprop="articleBody"]//p/text()').extract()
        except IndexError:
            pass
                
        text='\n'.join(text)
        

        return text


    def parse_subtopic(self):
        subtopics = ""
        try:
            subtopics = self.response.xpath('//meta[@property="article:tag"]/@content')[1].extract()
            subtopics= subtopics.lower()
            subtopics = re.sub(r'&#\d;|&.*;','',subtopics)
        except IndexError:
            pass

        return subtopics

    def parse_topic(self):
        
        topic = ''
        subtopics = ''
        topics = ''
        try:

            topics = self.response.xpath('//meta[@property="article:section"]/@content')[0].extract()
            topics= topics.lower()
        except IndexError:
            pass
        try:
            subtopics = self.response.xpath('//meta[@property="article:tag"]/@content')[1].extract()
            subtopics= subtopics.lower()
        except IndexError:
            pass
            
        T = self.T
        
        if subtopics or topics:
            topic = "others"
            if subtopics in T:
                topic = subtopics
            elif topics in T:
                topic = topics
        
        return topic
                    
            
        
                
    def parse_pdate(self):
        pdate = ""
        try:
            pdate = self.response.xpath('//meta[@property="article:published_time"]/@content')[0].extract()
            
        except IndexError:
            pass
        if pdate:
            pdate = datetime.datetime.strptime(pdate,'%Y-%m-%dT%H:%M:%S%z')
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
        
      
        return pdate

    def parse_ptimestamp(self):
        ptimestamp = ""
        pdate = ""
        try:
            pdate = self.response.xpath('//meta[@property="article:published_time"]/@content')[0].extract()
        except IndexError:
            pass
        if pdate:
            pdate=datetime.datetime.strptime(pdate,'%Y-%m-%dT%H:%M:%S%z')
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
            ptimestamp = int(mktime(pdate.timetuple()))
        return ptimestamp




