from time import strftime,strptime,mktime
import datetime
from scrapy_news.rules.rules import ArticleRules
import pytz
import re

class ReutersRule(ArticleRules):
    def __init__(self,response):
        self.response = response

    def parse_id(self):
        doc_id = ""
        try:
            doc_id = self.response.xpath('//meta[@property="og:url"]/@content')[0].extract()
            doc_id = re.match(r".*-id(.*)$", doc_id).group(1)
        except IndexError:
            pass
        return doc_id

    def parse_title(self):
        title = ""
        try:
            title = self.response.xpath('//meta[@property="og:title"]/@content')[0].extract()
        except IndexError:
            pass
        return title

    def parse_description(self):
        description = ""
        try:
            description = self.response.xpath('//meta[@name="description"]/@content')[0].extract()
        except IndexError:
            pass
        return description

    def parse_url(self):
        url = ""
        try:
            url = self.response.xpath('//meta[@property="og:url"]/@content')[0].extract()
        except IndexError:
            pass
        return url

    def parse_tags(self):
        tags = ""
        try:
            tags = self.response.xpath('//meta[@property="og:article:tag"]/@content')[0].extract()
            tags = tags.split(",")
        except IndexError:
            pass
        return tags

    def parse_language(self):
        language = ""
        try:
            language = self.response.xpath('//meta[@property="og:locale"]/@content')[0].extract()
        except IndexError:
            pass
        return language

    def parse_image(self):
        image = ""
        try:
            image = self.response.xpath('//link[@rel="image_src"]/@href')[0].extract()
        except IndexError:
            pass
        return image

    def parse_text(self):
        text = ""
        try:
            text = self.response.xpath('//span[@id="article-text"]//p//text()').extract()
            text='\n'.join(text)
        except IndexError:
            pass
        if not text:
            try:
                text = self.response.xpath('//div[@class="ArticleBody_body_2ECha"]//p//text()').extract()
                text='\n'.join(text)
            except IndexError:
                pass
                

            

            

        return text

    def parse_subtopic(self):
        subtopics = ""
        try:
            subtopics = self.response.xpath('//meta[@name="DCSext.ContentChannel"]/@content')[0].extract()
            subtopics = subtopics.lower()
            if subtopics == 'ousivMolt':
                subtopics = 'business'
            else:
                subtopics = subtopics
        except IndexError:
            pass



        return subtopics

    def parse_topic(self):
        topic = ""
        topics = ""
        subtopics = ""
        sub = False
        try:
            topics = self.response.xpath('//span[@class="article-section"]/a/text()')[0].extract()
            topics = topics.lower()
            subtopics = self.response.xpath('//meta[@name="DCSext.ContentChannel"]/@content')[0].extract()
            subtopics = subtopics.lower()
                        
        except IndexError:
            pass
        if not topics:
            try:
                topics = self.response.xpath('//meta[@property="og:article:section"]/@content')[0].extract()
                topics = topics.lower()
                subtopics = self.response.xpath('//meta[@name="DCSext.ContentChannel"]/@content')[0].extract()
                subtopics = subtopics.lower()
            except IndexError:
                pass
        T = self.T
        if subtopics or topics:
            topic = "others"
            for t in T:
                
                if t in subtopics:
                    topic = t
                elif t in topics:
                    topic = t
            
            
       

        return topic
                
    def parse_pdate(self):
        pdate = ""
        try:
            pdate = self.response.xpath('//meta[@name="REVISION_DATE"]/@content')[0].extract()
            pdate = datetime.datetime.strptime(pdate,'%a %b %d %H:%M:%S %Z %Y')
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
        except IndexError:
            pass
        
            
        return pdate

    def parse_ptimestamp(self):
        ptimestamp = ""
        try:
            pdate = self.response.xpath('//meta[@name="REVISION_DATE"]/@content')[0].extract()
            pdate = datetime.datetime.strptime(pdate,'%a %b %d %H:%M:%S %Z %Y')
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
            ptimestamp = int(mktime(pdate.timetuple()))
        except IndexError:
            pass
        

        return ptimestamp
