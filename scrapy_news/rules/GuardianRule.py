from time import strftime,strptime,mktime
import datetime
from dateutil.parser import parse
from scrapy_news.rules.rules import ArticleRules
import pytz


class GuardianRule(ArticleRules):
    def __init__(self,response):
        self.response = response
   
    def parse_id(self):
        doc_id = ""
        try:
            doc_id = self.response.xpath('//meta[@property="og:url"]/@content')[0].extract()
        except IndexError:
            pass
        return doc_id

    def parse_title(self):
        title = ""
        try:
            title = self.response.xpath('//meta[@property="og:title"]/@content')[0].extract()
        except IndexError:
            pass
        return title

    def parse_description(self):
        description = ""
        try:
            description = self.response.xpath('//meta[@property="og:description"]/@content')[0].extract()
        except IndexError:
            pass
        return description

    def parse_url(self):
        url = ""
        try:
            url = self.response.xpath('//meta[@property="og:url"]/@content')[0].extract()
        except IndexError:
            pass
        return url

    def parse_tags(self):
        tags = ""
        try:
            tags = self.response.xpath('//meta[@property="article:tag"]/@content')[0].extract()
            tags=tags.split(",")
        except IndexError:
            pass
        return tags
    #nofound

    def parse_language(self):
        language = "en-UK"
        return language

    def parse_image(self):
        image = ""
        try:
            image = self.response.xpath('//img[@class="maxed responsive-img"]/@src')[0].extract()
        except IndexError:
            pass
        return image

    def parse_text(self):
        text = ""
        try:
            text = self.response.xpath('//div[@class="content__article-body from-content-api js-article__body"]//p/text()').extract()
        except IndexError:
            pass
                
        text='\n'.join(text)
        

        return text


    def parse_subtopic(self):
        subtopics = ""
        try:
            subtopics = self.response.xpath('//li[@class="signposting__item signposting__item--current"]//a//text()')[0].extract()
            subtopics=subtopics.lower()
        except IndexError:
            pass

        return subtopics

    def parse_topic(self):
        topics = ''
        topic = ''
        subtopics = ''
        try:
            topics = self.response.xpath('//li[@class="signposting__item signposting__item--parent"]//a//text()')[0].extract()
            topics = topics.lower()
            subtopics = self.response.xpath('//li[@class="signposting__item signposting__item--current"]//a//text()')[0].extract()
            subtopics=subtopics.lower()
        except IndexError:
            pass
            
        T = self.T
        if topics:

            if topics == "sport":
                topics = "sports"
            elif topics == "tech":
                topics == "technology"
        
        
        if subtopics or topics:
            topic = "others"
            if subtopics in T:
                topic = subtopics
            elif topics in T:
                topic = topics

                
        
        return topic
                
    def parse_pdate(self):
        pdate = ""
        try:
            pdate = self.response.xpath('//meta[@property="article:published_time"]/@content')[0].extract()
            pdate=parse(pdate)
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
        except IndexError:
            pass
        
        
        return pdate

    def parse_ptimestamp(self):
        ptimestamp = ""
        try:
            pdate = self.response.xpath('//meta[@property="article:published_time"]/@content')[0].extract()
            pdate=parse(pdate)
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
            ptimestamp = int(mktime(pdate.timetuple()))
        except IndexError:
            pass

        return ptimestamp



