from time import strftime,strptime,mktime
import datetime
import pytz
from dateutil.parser import parse
from scrapy_news.rules.rules import ArticleRules


class NYTimesRule(ArticleRules):
    def __init__(self,response):
        self.response = response

    def parse_id(self):
        doc_id = "" 
        try:
            doc_id = self.response.xpath('//meta[@name="articleid"]/@content')[0].extract()
        except IndexError:
            pass
        return doc_id

    def parse_title(self):
        title = ""
        try:
            title = self.response.xpath('//meta[@property="og:title"]/@content')[0].extract()
        except IndexError:
            pass
        return title

    def parse_description(self):
        description = ""
        try:
            description = self.response.xpath('//meta[@name="description"]/@content')[0].extract()
        except IndexError:
            pass
        return description

    def parse_url(self):
        url = ""
        try:
            url = self.response.xpath('//meta[@property="og:url"]/@content')[0].extract()
        except IndexError:
            pass
        return url

    def parse_tags(self):
        tags = ""
        try:
            tags = self.response.xpath('//meta[@name="keywords"]/@content')[0].extract()
            tags = tags.split(",")
        except IndexError:
            pass
        return tags

    def parse_language(self):
        language = ""
        try:
            language = self.response.xpath('//meta[@itemprop="inLanguage"]/@content')[0].extract()
        except IndexError:
            pass
        return language

    def parse_image(self):
        image = ""
        try:
            image = self.response.xpath('//meta[@property="og:image"]/@content')[0].extract()
        except IndexError:
            pass
        return image

    def parse_text(self):
        text = ""
        try:
            text = self.response.xpath('//p[@class="story-body-text story-content"]//text()').extract()
        except IndexError:
            pass

        if not text:
            text = self.response.xpath('//div[@class="articleBody"]/p//text()').extract()
   
        if text:       
            text='\n'.join(text)
            text=text.replace("&mdash","")

        return text

    def parse_subtopic(self):
        subtopics = ""
        try:
            subtopics = self.response.xpath('//meta[@itemprop="articleSection"]/@content')[0].extract()
            subtopics = subtopics.lower()
        except IndexError:
            pass

        return subtopics

    def parse_topic(self):
        topic = ""
        topics = ""
        subtopics = ""
        try:
            topics = self.response.xpath('//meta[@property="article:top-level-section"]/@content')[0].extract()
            topics=topics.lower()
        except IndexError:
            pass
        try:
            subtopics = self.response.xpath('//meta[@itemprop="articleSection"]/@content')[0].extract()
            subtopics=subtopics.lower()
        except IndexError:
            pass    




        T = self.T
        if topics and subtopics:
            if subtopics in T:

                topic = subtopics
            else:
                topic = topics
                if topic not in T:
                    topic = "others"

            
                
        else:
            if subtopics:
                if subtopics in T:
                    topic = subtopics
                else:
                    topic = "others"
        return topic
                
    def parse_pdate(self):
        pdate = ""
        try:
            pdate = self.response.xpath('//meta[@itemprop="datePublished"]/@content')[0].extract()
            if len(pdate) < 12:
                pdate=''.join([pdate,"00:00:00"])
                pdate=datetime.datetime.strptime(pdate,'%Y-%m-%d%H:%M:%S')
                pdate=pytz.timezone("America/New_York").localize(pdate)
                tz = pytz.timezone("UTC")
                pdate = pdate.astimezone(tz)
            else:
                pdate=parse(pdate)
                tz = pytz.timezone("UTC")
                pdate = pdate.astimezone(tz)
        except IndexError:
            pass

        return pdate

    def parse_ptimestamp(self):
        ptimestamp = ""
        pdate = ""
        try:
            pdate= self.response.xpath('//meta[@itemprop="datePublished"]/@content')[0].extract()
            if pdate:
                if len(pdate) < 12:
                    pdate=''.join([pdate,"00:00:00"])
                    pdate=datetime.datetime.strptime(pdate,'%Y-%m-%d%H:%M:%S')
                    pdate=pytz.timezone("America/New_York").localize(pdate)
                    tz = pytz.timezone("UTC")
                    pdate = pdate.astimezone(tz)
                else:
                    pdate=parse(pdate)
                    tz = pytz.timezone("UTC")
                    pdate = pdate.astimezone(tz)

                ptimestamp = int(mktime(pdate.timetuple()))
        except IndexError:
            pass

        return ptimestamp
