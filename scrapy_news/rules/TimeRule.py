from scrapy_news.rules.rules import ArticleRules
from time import strftime,strptime,mktime
import datetime
import re
import json
import pytz



class TimeRule(ArticleRules):
    def __init__(self,response):
        self.response = response
   
 
    def parse_id(self):
        doc_id = ""
        try:
            doc_id = self.response.xpath('//meta[@property="og:url"]/@content')[0].extract()
            #doc_id = doc_id.split('/')[3]
        except IndexError:
            pass
        return doc_id

    def parse_title(self):
        title = ""
        try:
            title = self.response.xpath('//meta[@property="og:title"]/@content')[0].extract()
        except IndexError:
            pass
        return title

    def parse_description(self):
        description = ""
        try:
            description = self.response.xpath('//meta[@name="description"]/@content')[0].extract()
        except IndexError:
            pass
        return description

    def parse_url(self):
        url = ""
        try:
            url = self.response.xpath('//meta[@property="og:url"]/@content')[0].extract()
        except IndexError:
            pass
        return url

    def parse_tags(self):
        tags = ""
        try:
            tags = self.response.xpath('//meta[@name="keywords"]/@content')[0].extract()
            tags = tags.split(",")
        except IndexError:
            pass
        return tags

    def parse_language(self):
        language = ""
        try:
            language = self.response.xpath('//meta[@property="og:locale"]/@content')[0].extract()
        except IndexError:
            pass
        return language

    def parse_image(self):
        image=""
        try:    
            image = self.response.xpath('//div[@class="row"]/picture/img/@src')[0].extract()
        except IndexError:
            pass
        return image

    def parse_text(self):
        text = ""
        try:
            content  = self.response.xpath('//script[@type="application/ld+json"]/text()')[0].extract()
            data = json.loads(content)
            text = data['articleBody']
            text = text.replace('You are getting a free preview of a TIME Magazine article from our archive. Many of our articles are reserved for subscribers only. Want access to more subscriber-only content, click here to subscribe. ','')
            text = re.sub(r'\&\#\d{4}','',text)
        except IndexError:
            pass
        return text

    def parse_subtopic(self):
        subtopics = ""
        try:
            subtopics = self.response.xpath('//div[@class="row"]/a[@class="text size-1x-small font-accent color-brand all-caps"]/text()')[0].extract()
            subtopics = subtopics.lower()
        except IndexError:
            pass


        return subtopics

    def parse_topic(self):
        topic = ""
        subtopics = ""
        topics = ""
        try:
            content  = self.response.xpath('//script[@type="application/ld+json"]/text()')[0].extract()
        
            data = json.loads(content)
            topics = data['articleSection']
            topics = topics.lower()
        except IndexError:
            pass

        try:
            subtopics = self.response.xpath('//a[@class="text size-1x-small font-accent color-brand all-caps"]/text()')[0].extract()
            subtopics = subtopics.lower()
        except IndexError:
            pass
            
        T = self.T
        if subtopics or topics:
            topic = "others"
            if subtopics in T:
                topic = subtopics
            elif topics in T:
                topic = topics
        
            
                
           

                
            
        return topic
                
    def parse_pdate(self):
        pdate = ""
        try:
            content  = self.response.xpath('//script[@type="application/ld+json"]/text()')[0].extract()
        
            data = json.loads(content)
            pdate = data['datePublished']
            pdate = ''.join([pdate,'GMT'])
            pdate = datetime.datetime.strptime(pdate,'%Y-%m-%dT%H:%M:%S%Z')
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
        except IndexError:
            pass
        
        return pdate

    def parse_ptimestamp(self):
        ptimestamp = ""
        try:
            content  = self.response.xpath('//script[@type="application/ld+json"]/text()')[0].extract()
        
            data = json.loads(content)
            pdate = data['datePublished']
            pdate = ''.join([pdate,'GMT'])
            pdate = datetime.datetime.strptime(pdate,'%Y-%m-%dT%H:%M:%S%Z')
            tz = pytz.timezone("UTC")
            pdate = pdate.astimezone(tz)
  

            ptimestamp = int(mktime(pdate.timetuple()))
        except IndexError:
            pass


        return ptimestamp   
