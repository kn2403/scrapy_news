from abc import ABCMeta, abstractmethod

class ArticleRules(metaclass=ABCMeta):
    T = ["economy","finance","social","legal","governance","market","business","politics","technology","sports","health","world","environment"]

    @abstractmethod
    def __init__(self,response):
        pass
    @abstractmethod
    def parse_id(self):
            
        pass

    @abstractmethod
    def parse_title(self):
        pass
    @abstractmethod
    def parse_description(self):
        pass
    @abstractmethod
    def parse_url(self):
        pass
    @abstractmethod
    def parse_tags(self):
        pass
    @abstractmethod
    def parse_language(self):
        pass
    @abstractmethod
    def parse_image(self):
        pass
    @abstractmethod
    def parse_text(self):
        pass
    @abstractmethod
    def parse_subtopic(self):
        pass
    @abstractmethod
    def parse_topic(self):
        pass
    @abstractmethod           
    def parse_pdate(self):
        pass
    @abstractmethod
    def parse_ptimestamp(self):
        pass
            

 
