# -*- coding: utf-8 -*-
from scrapy.http import Request
from scrapy_news.items import NewsItem
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor
from time import strftime,strptime,mktime
import datetime
from scrapy_news.rules.GuardianRule import GuardianRule


class GuardianNewSpider(CrawlSpider):
    """Spider for scraping theguardian.com"""
    name = "guardian"
    allowed_domains = ["theguardian.com"]
    start_urls = ["https://www.theguardian.com/index/subjects",
                  ]
    rules = [
        
        Rule(LinkExtractor(allow=r"https://www.theguardian.com/index/subjects/.*",restrict_xpaths = '//li[@class="linkslist__item"]'),callback = "request_part", follow=False),
    ]

    def __init__(self,**kw):
        super(GuardianNewSpider, self).__init__(**kw)
        self.log_every = kw.get('log_every')


    def parse_article(self,response):
        """Function to scrap each source page"""
        item = NewsItem()
        rule = GuardianRule(response)
        item['doc_id'] = rule.parse_id()
        item['title'] = rule.parse_title()
        item['description'] = rule.parse_description()
        item['url'] = rule.parse_url()
        item['text'] = rule.parse_text()
        
        item['source_type'] = "news"
        item['source_name'] = "guardian"
        item['domain'] = "theguardian.com"
            
        item['tags'] = rule.parse_tags()
        item['language'] = rule.parse_language()
        item['source_country'] = "GBR"
        item['image'] = rule.parse_image()

        item['topic'] = rule.parse_topic()
        item['subtopic'] = rule.parse_subtopic()
        
        item['published_date']=rule.parse_pdate()
        item['published_timestamp']=rule.parse_ptimestamp()


        cdate = datetime.datetime.utcnow()
        cdates=cdate.strftime('%a %b %d %H:%M:%S UTC %Y')
        item['crawled_date']=datetime.datetime.strptime(cdates,'%a %b %d %H:%M:%S %Z %Y')
        item['crawled_timestamp']=int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function to request each aricle page"""
        items = []
        items = response.xpath('//div[@class="fc-item__container"]//a/@href').extract()
        
        for item in items:
            yield Request(item,callback = self.parse_article)
        
        
        current = ""
        try:
            current = int(response.xpath('//span[@aria-label="Current page"]/text()')[0].extract())
        except IndexError:
            pass

        if current:
            next_page = current + 1
            next_page = str(next_page )
            url = ""
            try:
                query = ''.join(['//div[@class="pagination__list"]//a[@data-page="',next_page,'"]/@href'])
        
                url = response.xpath(query)[0].extract()
            except IndexError:
                pass
            if url:

                return Request(url, callback = self.request_article)


    def request_part(self,response):
        """Function to scrap each subject page that contains links of articles"""
        links = []
        links = response.xpath('//li[@class="linkslist__item"]/a/@href').extract()
        for link in links:

            yield Request(link,callback = self.request_article)

