from scrapy.http import Request
from scrapy_news.items import NewsItem
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor
from time import strftime,strptime,mktime
import datetime
import re
import json
from scrapy_news.rules.TimeRule import TimeRule

class TimeNewSpider(CrawlSpider):
    """Spider for scraping time.com"""
    name = "time"
    allowed_domains = ["time.com"]
    start_urls = ["http://time.com/html-sitemap/",
                  ]
    rules = [
               Rule(LinkExtractor(allow=r'http://time.com/html-sitemap/time-section-[a-z]*/',restrict_xpaths = '//div[@class="ti-sitemap-list clearfix"]'),callback = "request_part", follow=False),
    ]



    def __init__(self,**kw):
        super(TimeNewSpider, self).__init__(**kw)
        self.log_every = kw.get('log_every')
    def parse_article(self,response):
        """Function to scrap each source page"""
        item = NewsItem()
        item['source_type'] = "news"
        item['source_name'] = "time"
        item['domain'] = "time.com"
        item['source_country'] = "USA"

        
        rule = TimeRule(response)
        item['doc_id'] = rule.parse_id()
        item['title'] = rule.parse_title()
        item['description'] = rule.parse_description()
        item['url'] = rule.parse_url()
        item['text']= rule.parse_text()
        

            
        item['tags'] = rule.parse_tags()
        item['language'] = rule.parse_language()

        item['image'] = rule.parse_image()

        item['topic'] = rule.parse_topic()
        item['subtopic'] = rule.parse_subtopic()
        
        item['published_date']=rule.parse_pdate()
        item['published_timestamp']=rule.parse_ptimestamp()

        
        
        cdate = datetime.datetime.utcnow()
        cdates = cdate.strftime('%a %b %d %H:%M:%S UTC %Y')
        item['crawled_date'] = datetime.datetime.strptime(cdates,'%a %b %d %H:%M:%S %Z %Y')
        item['crawled_timestamp'] = int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function request each article page"""
        items = []
        items = response.xpath('//div[@class="ti-sitemap-list clearfix"]/ul/li/a/@href').extract()
        
        for item in items:
            yield Request(item,callback = self.parse_article)

    def request_part(self,response):
        """Function request each part page that contains article links"""
        links = []
        links = response.xpath('//div[@class="ti-sitemap-list clearfix"]/ul/li/a/@href').extract()
        for link in links:
            

            yield Request(link,callback = self.request_article)
