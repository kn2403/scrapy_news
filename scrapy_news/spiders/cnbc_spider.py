from scrapy.http import Request
from scrapy_news.items import NewsItem
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor
from time import strftime,strptime,mktime
import datetime
from scrapy_news.rules.CNBCRule import CNBCRule


class CNBCNewSpider(CrawlSpider):
    """Spider for scraping CNBC.com"""
    name = "cnbc"
    allowed_domains = ["cnbc.com"]
    start_urls = ["http://www.cnbc.com/site-map/",
                  ]

    dlinks=(			 
             '//www.cnbc.com/live-tv/',
	     '//www.cnbc.com/latest-video/',
	     '//www.cnbc.com/business-day/',
	     '//www.cnbcprime.com/',

	     '//www.cnbc.com/video-ceo-interviews/',
	     '//www.cnbc.com/video-analyst-interviews/',
             '//www.cnbc.com/quotes/',	 
	     '//www.cnbc.com/markets-europe/', 
	     '//www.cnbc.com/us-markets/',
	     '//www.cnbc.com/pre-markets/',
	     '//www.cnbc.com/stocks/',
	     '//www.cnbc.com/bonds/',
	     '//www.cnbc.com/commodities/',
	     '//www.cnbc.com/currencies/',
	     '//www.cnbc.com/exchange-traded-funds/',
	     '//www.cnbc.com/mutual-funds/',
             '//www.cnbc.com/world/',
	     '//www.cnbc.com/markets/',

            )

    rules = [
        
        Rule(LinkExtractor(allow=r"//www.cnbc.com/.*",deny=dlinks,restrict_xpaths = '//div[@class="story"]'),callback = "request_article", follow=False),
    ]
    def __init__(self,**kw):
        super(CNBCNewSpider, self).__init__(**kw)
        self.log_every = kw.get('log_every')



    def parse_article(self,response):
        """Function to scrap each source page"""
        item = NewsItem()
        ogtype = response.xpath('//meta[@property="og:type"]/@content')[0].extract()
        if ogtype == "video":
            pass
        else:
            
            rule = CNBCRule(response)
            item['doc_id'] = rule.parse_id()
            item['title'] = rule.parse_title()
            item['description'] = rule.parse_description()
            item['url'] = rule.parse_url()
            item['text'] = rule.parse_text()
        
            item['source_type'] = "news"
            item['source_name'] = "CNBC"
            item['domain'] = "cnbc.com"
            
            item['tags'] = rule.parse_tags()
            item['language'] = rule.parse_language()
            item['source_country'] = "US"
            item['image'] = rule.parse_image()

            item['topic'] = rule.parse_topic()
            item['subtopic'] = rule.parse_subtopic()
        
            item['published_date']=rule.parse_pdate()
            item['published_timestamp']=rule.parse_ptimestamp()


            cdate = datetime.datetime.utcnow()
            cdates=cdate.strftime('%a %b %d %H:%M:%S UTC %Y')
            item['crawled_date']=datetime.datetime.strptime(cdates,'%a %b %d %H:%M:%S %Z %Y')
            item['crawled_timestamp']=int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function to request each aricle page"""
        items = []
        items = response.xpath('//ul[@class="stories_assetlist"]//a/@href').extract()
        
        for item in items:
            i = ''.join(["http://www.cnbc.com",item])
            yield Request(i,callback = self.parse_article)
        
        
        next_page = ""
        try:
            next_page = response.xpath('//div[@class="quickPagJump rightPagCol"]//a/@href')[0].extract()
        except IndexError:
            pass

        if next_page:
            url = ''.join(["http://www.cnbc.com",next_page])

            yield Request(url, callback = self.request_article)


 

