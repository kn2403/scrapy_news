from scrapy.http import Request
from scrapy_news.items import NewsItem
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor
from time import strftime,strptime,mktime
import datetime
import pytz
from dateutil.parser import parse
from scrapy_news.rules.NYTimesRule import NYTimesRule

class NYtimesNewSpider(CrawlSpider):
    """Spider for scraping nytimes.com"""
    name = "nytimes"
    allowed_domains = ["nytimes.com"]
    start_urls = ["http://spiderbites.nytimes.com/",
                  ]
    rules = [
        
        Rule(LinkExtractor(allow=r'free_198[7-9]/index.html|free_199[0-9]/index.html|free_20\d{2}/index.html',restrict_xpaths = '//div[@id="articles_free"]'),callback = "request_part", follow=False),
    ]

    def __init__(self,**kw):
        super(NYtimesNewSpider, self).__init__(**kw)
        self.log_every = kw.get('log_every')


    def parse_article(self,response):
        """Function to scrap each source page"""
        item = NewsItem()
        rule = NYTimesRule(response)
        item['doc_id'] = rule.parse_id()
        item['title'] = rule.parse_title()
        item['description'] = rule.parse_description()
        item['url'] = rule.parse_url()
        item['text'] = rule.parse_text()
        
        item['source_type'] = "news"
        item['source_name'] = "nytimes"
        item['domain'] = "nytime.com"
            
        item['tags'] = rule.parse_tags()
        item['language'] = rule.parse_language()
        item['source_country'] = "USA"
        item['image'] = rule.parse_image()

        item['topic'] = rule.parse_topic()
        item['subtopic'] = rule.parse_subtopic()
        
        item['published_date']=rule.parse_pdate()
        item['published_timestamp']=rule.parse_ptimestamp()


        cdate = datetime.datetime.utcnow()
        cdates=cdate.strftime('%a %b %d %H:%M:%S UTC %Y')
        item['crawled_date']=datetime.datetime.strptime(cdates,'%a %b %d %H:%M:%S %Z %Y')
        item['crawled_timestamp']=int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function to request each aricle page"""
        items = []
        items = response.xpath('//div[@id="mainContent"]//a/@href').extract()
        
        for item in items:
            yield Request(item,callback = self.parse_article)

    def request_part(self,response):
        """Function to scrap part page that contains links of articles"""
        links = []
        links = response.xpath('//div[@class="articlesMonth"]//a/@href').extract()
        for link in links:
            link=''.join(["http://spiderbites.nytimes.com",link]) 

            yield Request(link,callback = self.request_article)
