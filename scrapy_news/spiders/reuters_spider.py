from scrapy.http import Request
from scrapy_news.items import NewsItem
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor
from time import strftime,strptime,mktime
import datetime
from scrapy_news.rules.ReutersRule import ReutersRule

class ReutersNewSpider(CrawlSpider):
    """Spider for scraping reuters.com"""
    name = "reuters"
    allowed_domains = ["www.reuters.com"]
    start_urls = ["http://www.reuters.com/resources/archive/us",
                  ]
    rules = [
        Rule(LinkExtractor(allow = r'resources/archive/us/\d{8}.html'),callback = "request_article", follow  =False),
        Rule(LinkExtractor(allow = r'resources/archive/us/\d{4}.html',restrict_xpaths = '//div[@class="moduleBody"]/h4'),callback = "request_part", follow = False),
    ]


    def __init__(self,**kw):
        super(ReutersNewSpider, self).__init__(**kw)
        self.log_every = kw.get('log_every')

    def parse_article(self,response):
        """Function to scrap each source page"""
        item = NewsItem()
        rule = ReutersRule(response)        
        item['source_type'] = "news"
        item['source_name'] = "reuters"
        item['domain'] = "reuters.com"
        item['source_country'] = "USA"
        item['text']= rule.parse_text()

        item['doc_id'] = rule.parse_id()
        item['title'] = rule.parse_title()
        item['description'] = rule.parse_description()
        item['url'] = rule.parse_url()
        
            
        item['tags'] = rule.parse_tags()
        item['language'] = rule.parse_language()
        item['source_country'] = "USA"
        item['image'] = rule.parse_image()

        item['topic'] = rule.parse_topic()
        item['subtopic'] = rule.parse_subtopic()
        
        item['published_date']=rule.parse_pdate()
        item['published_timestamp']=rule.parse_ptimestamp()

        cdate = datetime.datetime.utcnow()
        cdates = cdate.strftime('%a %b %d %H:%M:%S UTC %Y')
        item['crawled_date'] = datetime.datetime.strptime(cdates,'%a %b %d %H:%M:%S %Z %Y')
        item['crawled_timestamp'] = int(cdate.timestamp())

        return item

    def request_article(self, response):
        """Function to request each source page"""
        items = []
        items = response.xpath('//div[@class="module"]/div[@class="headlineMed"]/a/@href').extract()
        
        for item in items:
            yield Request(item,callback = self.parse_article)

    def request_part(self,response):
        """Function to request each page that contains links of article pages"""
        links = []
        links = response.xpath('//div[@class="moduleBody"]/p/a/@href').extract()
        for link in links:
            l = ''.join(["http://www.reuters.com",link])

            yield Request(l,callback = self.request_article)


