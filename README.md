### Introduction ###

* A scrapy project used to scrapy news from reuters.com, time.com, nytimes.com, theguardian.com and cnbc.com, saving the information to mongoDB


### Instruction ###

* Set directory to the scrapy_news file, run command: python run_spider.py [spider name], please make sure mongod is running.

* Enter Username and Password in settings_template.py and save it as settings.py